# README #

Código fuente para la prueba de concepto de la publicación ["Experiencia en implementación de un proceso de despliegue continuo en un sistema basado en Wordpress"](http://www.institucional.frc.utn.edu.ar/sistemas/lidicalso/pub/file/Publicaciones/JAIIO2017_despliegue_continuo_sistema_basado_wordpress.pdf)

# Cómo comenzar #

## Instalar Docker ##
1. Descargar e instalar Docker
    1. Guía de instalación: https://docs.docker.com/engine/installation/
    1. Descargas: https://download.docker.com/

## Iniciar el entorno ##
1. Abrir la ventana de comandos y ubicarse en el directorio de este repositorio
2. Verificar que las imágenes de Docker son descargadas correctamente: docker-compose pull
3. Iniciar el entorno: docker-compose up -d

## Inicializar el sitio Wordpress local ##
1. Abrir el navegador e ingresar a http://localhost
2. Seguir las instrucciones para iniciar el sitio Wordpress
3. Verificar que los archivos del sitio wordpress han aparecido en el directorio "wordpress"

## Versionar la primera versión del sitio ##
1. Incluir los archivos del sitio en el control de versiones:
    1. git add wordpress
    2. git commit -m "site initialization"
2. Extraer la base de datos con la utilidad de configuración track-db.sh.
    1. No requiere ningún parámetro, basta con invocarla directamente: ./track-db.sh
    2. Verificar que los archivos de extracción de la base de datos fueron creados en el directorio config/backup/{data,structure}
3. Incluir los archivos de extracción de la base de datos:
    1. git add config/backup
    2. git commit -m "added data base"

## Versionado del sitio luego de una modificación ##
1. Generar un nuevo post en el sitio.
2. Verificar que no hay cambios en control de versionado:
    1. git status
    2. NOTA: si se agregan imágenes al post del sitio, estas imágenes van a aparecer como archivos pendientes de ser versionados, pero no el contenido del post.
3. Extraer la base de datos con la utilidad de configuración track-db.sh.
4. Verificar que han aparecido nuevos cambios en el control de versionado:
    1. git status
5. Incluir los cambios obtenidos con la utilidad track-db.sh:
    1. git add config/backup
    2. git commit -m "added a post"