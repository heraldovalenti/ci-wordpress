#!/bin/bash

docker-compose down
rm -rf config/backup/tables.txt config/backup/data/* config/backup/structure/* wordpress/* wordpress/.* db/*